const endpointCourses = "https://careerfoundry.com/en/api/courses";

fetch(`${endpointCourses}`, {
  method: "GET",
})
.then(responseCourses => responseCourses.json())
.then((courses) => {
  const courseList = courses.courses;

  for (var name in courseList) {
    if (courseList.hasOwnProperty(name)) {
      $(".courses-list .grid").append(
        "<div class='col-lg-4 col-sm-6'>"
          + "<div class='course' id="+'course-'+ name +" style="+'background-image:url(/assets/img/'+name+'.jpg'+')' +">"
            + "<div class='title'>"
              + courseList[name].title
            + "</div>"

            + "<div class='start-date'> Next start date:"
              + courseList[name].next_start_formatted
            + "<div>"

            + "<div class='show-more' id="+ name +">Show Details</div>"
          + "</div>"
        + "</div>"
      );
    }
  }

  $(".show-more").on("click", function(){
    $(this).hide();
    var getId = $(this).attr('id');

    const endpointCourse = "https://careerfoundry.com/en/api/courses/" + getId

    fetch(`${endpointCourse}`, {
      method: "GET",
    })
    .then(responseCourse => responseCourse.json())
    .then((course) => {

      $("#course-"+getId).append(
        "<div class='next-course'>" + course.start_dates[1] + "<br/>" + course.start_dates[2] + "</div>"
        + "<div class='price'>" + 'Total price:' + course.price.EU.total + "</div>"
      );

    });
  });
})